package pidev.services.training;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pidev.domain.Training;

/**
 * Session Bean implementation class TrainingServiceEjb
 * 
 * Cette classe implémente tous les méthodes associées à un Training
 * 
 * @author Fedi
 */
@Stateless
@LocalBean
public class TrainingServiceEjb implements TrainingServiceEjbRemote,
		TrainingServiceEjbLocal {

	@PersistenceContext(unitName = "pidev")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public TrainingServiceEjb() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addTraining(Training training) {
		em.persist(training);
	}

	@Override
	public void updateTraining(Training training) {
		em.merge(training);
	}

	@Override
	public Training findByIdTraining(int idTraining) {
		return em.find(Training.class, idTraining);
	}

	@Override
	public void deleteTraining(Training training) {
		em.remove(training);
	}

	@Override
	public List<Training> findAll() {
		return em.createQuery("select t from Training t").getResultList();
	}

}
