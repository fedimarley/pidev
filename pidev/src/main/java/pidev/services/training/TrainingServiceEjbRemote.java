package pidev.services.training;

import java.util.List;

import javax.ejb.Remote;

import pidev.domain.Training;

/**
 * Declaration des methodes associées a un Training
 * 
 * @author Fedi
 *
 */

@Remote
public interface TrainingServiceEjbRemote {

	/**
	 * cette methode permet d'ajouter un nouveau Training
	 * 
	 * @param training
	 */
	public void addTraining(Training training);

	/**
	 * cette methode permet de modifier un Training
	 * 
	 * @param training
	 */
	public void updateTraining(Training training);

	/**
	 * cette methode permet de chercher un Training en utilisant l'Id
	 * 
	 * @param idTraining
	 * @return Training
	 */
	public Training findByIdTraining(int idTraining);

	/**
	 * cette methode permet de supprimer un Training
	 * 
	 * @param Training
	 */
	public void deleteTraining(Training Training);
	
	public List<Training> findAll();
}
