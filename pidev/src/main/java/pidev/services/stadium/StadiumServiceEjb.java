package pidev.services.stadium;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pidev.domain.Stadium;

/**
 * Session Bean implementation class StadiumServiceEjb
 */
@Stateless
@LocalBean
public class StadiumServiceEjb implements StadiumServiceEjbRemote, StadiumServiceEjbLocal {
	
	@PersistenceContext(unitName = "pidev")
	private EntityManager em;

    /**
     * Default constructor. 
     */
    public StadiumServiceEjb() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addStadium(Stadium stadium) {
		
		em.persist(stadium);
		
	}

	@Override
	public void updateStadium(Stadium stadium) {
		em.merge(stadium);
		
	}

	@Override
	public Stadium findByIdStadium(int Id_Staduim) {
		Stadium s = em.find(Stadium.class, Id_Staduim);
		return s;
	}

	@Override
	public void deleteStadium(Stadium stadium) {
		em.remove(em.merge(stadium));
		
	}

}
