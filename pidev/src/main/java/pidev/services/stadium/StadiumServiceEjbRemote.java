package pidev.services.stadium;

import javax.ejb.Remote;

import pidev.domain.Stadium;

@Remote
public interface StadiumServiceEjbRemote {
	
	/**
	 * Ajouter un stade
	 * @param stadium
	 */
	public void addStadium(Stadium stadium);
	/**
	 * Modifier un stade
	 * @param stadium
	 */
	public void updateStadium(Stadium stadium);
	/**
	 * rechercher un stade avec l identifiant
	 * @param Id_Staduim
	 * @return
	 */
	public Stadium  findByIdStadium(int Id_Staduim);
	/**
	 * supprimer un stade
	 * @param stadium
	 */
	public void deleteStadium(Stadium stadium);
	

}
