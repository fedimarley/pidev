package pidev.services.association;

import java.util.List;

import javax.ejb.Remote;

import pidev.domain.Association;

@Remote
public interface AssociationServiceEJBRemote {
	/**
	 * cette methode permet d'ajouter une nouvelle association
	 * 
	 * @param association
	 */
	public void addAssociation(Association association);

	/**
	 * cette methode permet de modifier une Association
	 * 
	 * @param Association
	 */
	public void updateAssociation(Association association);

	/**
	 * cette methode permet de chercher un association en utilisant l'Id
	 * 
	 * @param idAdmin
	 * @return Association
	 */
	public Association findByIdAssociation(int idAssociation);

	/**
	 * cette methode permet de supprimer un Association
	 * 
	 * @param admin
	 */
	public void deleteAssociation(Association association);
	
	public List<Association> findAll();
}
