package pidev.services.association;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pidev.domain.Administrator;
import pidev.domain.Association;
import pidev.domain.Referee;

/**
 * Session Bean implementation class AssociationServiceEJB
 */
@Stateless
@LocalBean
public class AssociationServiceEJB implements AssociationServiceEJBRemote, AssociationServiceEJBLocal {

    /**
     * Default constructor. 
     */
	@PersistenceContext(unitName = "pidev")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public AssociationServiceEJB() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addAssociation(Association association) {
		em.persist(association);
	}

	@Override
	public void updateAssociation(Association association) {
		em.merge(association);
	}

	@Override
	public Association findByIdAssociation(int idAssociation) {
		return em.find(Association.class, idAssociation);
	}

	@Override
	public void deleteAssociation(Association association) {
		em.remove(em.merge(association));		
	}


	@Override
	public List<Association> findAll() {
		return em.createQuery("select a from Association a").getResultList();
	}
	
}
