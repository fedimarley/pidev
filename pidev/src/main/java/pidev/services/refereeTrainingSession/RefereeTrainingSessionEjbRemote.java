package pidev.services.refereeTrainingSession;

import java.util.List;

import javax.ejb.Remote;

import pidev.domain.RefereeTrainingSession;
import pidev.domain.TrainingSession;

@Remote
public interface RefereeTrainingSessionEjbRemote {
	/**
	 * this methode adds a Referee Training Session
	 * @param refereerainingSession
	 */
	public void addRefereeTrainingSession(RefereeTrainingSession refereeTrainingSession);
	/**
	 * this methode edit a Referee Training Session
	 * @param refereerainingSession
	 */
	public void updateRefereeTrainingSession(RefereeTrainingSession refereeTrainingSession);
	/**
	 * this methode find by id a Referee Training Session
	 * @param id
	 * @return
	 */
	public RefereeTrainingSession findRefereeTrainingSessionById(int id);
	/**
	 * this methode remove Referee Training Session
	 * @param refereerainingSession
	 */
	public void deleteRefereeTrainingSession(RefereeTrainingSession refereerainingSession);
	public List<RefereeTrainingSession> findAll();
}
