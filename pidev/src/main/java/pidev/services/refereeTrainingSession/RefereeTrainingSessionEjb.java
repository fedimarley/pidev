package pidev.services.refereeTrainingSession;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pidev.domain.RefereeTrainingSession;


/**
 * Session Bean implementation class RefereeTrainingSessionEjb
 */
@Stateless
@LocalBean
public class RefereeTrainingSessionEjb implements RefereeTrainingSessionEjbRemote, RefereeTrainingSessionEjbLocal {


	@PersistenceContext(unitName = "pidev")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public RefereeTrainingSessionEjb() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addRefereeTrainingSession(RefereeTrainingSession refereetrainingSession) {
			em.persist(refereetrainingSession);		
	}

	@Override
	public void updateRefereeTrainingSession(RefereeTrainingSession refereetrainingSession) {
		em.merge(refereetrainingSession);
		
	}

	@Override
	public RefereeTrainingSession findRefereeTrainingSessionById(int id) {
		RefereeTrainingSession r=em.find(RefereeTrainingSession.class,id);
	return r;
	}

	@Override
	public void deleteRefereeTrainingSession(RefereeTrainingSession refereetrainingSession) {
		em.remove(em.merge(refereetrainingSession));		
	}

	@Override
	public List<RefereeTrainingSession> findAll() {
		return em.createQuery("select r from RefereeTrainingSession r").getResultList();

	}

}
