package pidev.services.referee;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pidev.domain.Referee;

/**
 * Session Bean implementation class RefereeServiceEjb
 */
@Stateless
@LocalBean
public class RefereeServiceEjb implements RefereeServiceEjbRemote, RefereeServiceEjbLocal {

	
	@PersistenceContext(unitName = "pidev")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public RefereeServiceEjb() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addReferee(Referee referee) {
			em.persist(referee);		
	}

	@Override
	public void updateReferee(Referee referee) {
		em.merge(referee);
		
	}

	@Override
	public Referee findRefereeById(int id) {
	Referee r=em.find(Referee.class,id);
	return r;
	}

	@Override
	public void deleteReferee(Referee referee) {
		em.remove(em.merge(referee));		
	}

	@Override
	public List<Referee> findAll() {
		
		return em.createQuery("select r from Referee r").getResultList();
	}

}
