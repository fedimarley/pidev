package pidev.services.referee;

import java.util.List;

import javax.ejb.Remote;



import pidev.domain.Referee;

@Remote
public interface RefereeServiceEjbRemote {
	/**
	 * this methode adds a referee
	 * @param referee
	 */
	public void addReferee(Referee referee);
	/**
	 * this methode edit a referee
	 * @param referee
	 */
	public void updateReferee(Referee referee);
	/**
	 * this methode find by id a referee
	 * @param id
	 * @return
	 */
	public Referee findRefereeById(int id);
	/**
	 * this methode remove referee
	 * @param personne
	 */
	public void deleteReferee(Referee referee);
	
	public List<Referee> findAll();
}
