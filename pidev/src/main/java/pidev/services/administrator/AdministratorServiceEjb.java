package pidev.services.administrator;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import pidev.domain.Administrator;

/**
 * Session Bean implementation class AdministratorServiceEjb
 * 
 * Cette classe implémente tous les méthodes associées à un administrateur
 * 
 * @author Fedi
 */
@Stateless
public class AdministratorServiceEjb implements AdministratorServiceEjbRemote,
		AdministratorServiceEjbLocal {

	@PersistenceContext(unitName = "pidev")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public AdministratorServiceEjb() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addAdministrator(Administrator admin) {
		em.persist(admin);
	}

	@Override
	public void updateAdministrator(Administrator admin) {
		em.merge(admin);
	}

	@Override
	public Administrator findByIdAdministrator(int idAdmin) {
		return em.find(Administrator.class, idAdmin);
	}

	@Override
	public void deleteAdministrator(Administrator admin) {
		em.remove(admin);
	}

	@Override
	public List<Administrator> findAll() {
		return em.createQuery("select a from Administrator a",Administrator.class).getResultList();
	}
	
	@Override
	public Administrator getAdministratorByLogin(String name) {
	    TypedQuery<Administrator> query = em.createQuery("SELECT c FROM Administrator c WHERE c.login = :name", Administrator.class);
	    return query.setParameter("name", name).getSingleResult();
	  } 

}
