package pidev.services.administrator;

import java.util.List;

import javax.ejb.Local;

import pidev.domain.Administrator;

@Local
public interface AdministratorServiceEjbLocal {

	List<Administrator> findAll();

}
