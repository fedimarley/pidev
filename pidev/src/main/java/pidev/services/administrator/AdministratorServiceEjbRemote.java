package pidev.services.administrator;

import java.util.List;

import javax.ejb.Remote;

import pidev.domain.Administrator;

/**
 * Declaration des methodes associées a un administrateur
 * 
 * @author Fedi
 *
 */
@Remote
public interface AdministratorServiceEjbRemote {

	/**
	 * cette methode permet d'ajouter un nouveau administrateur
	 * 
	 * @param admin
	 */
	public void addAdministrator(Administrator admin);

	/**
	 * cette methode permet de modifier un administrateur
	 * 
	 * @param admin
	 */
	public void updateAdministrator(Administrator admin);

	/**
	 * cette methode permet de chercher un administrateur en utilisant l'Id
	 * 
	 * @param idAdmin
	 * @return Administrator
	 */
	public Administrator findByIdAdministrator(int idAdmin);

	/**
	 * cette methode permet de supprimer un administrateur
	 * 
	 * @param admin
	 */
	public void deleteAdministrator(Administrator admin);
	
	public List<Administrator> findAll();
	
	public Administrator getAdministratorByLogin(String name);
	
}
