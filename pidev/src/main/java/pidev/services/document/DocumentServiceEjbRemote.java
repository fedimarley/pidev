package pidev.services.document;

import java.util.List;

import javax.ejb.Remote;

import pidev.domain.Document;

/**
 * Declaration des methodes associées a un Document
 * 
 * @author Fedi
 *
 */

@Remote
public interface DocumentServiceEjbRemote {

	/**
	 * cette methode permet d'ajouter un nouveau Document
	 * 
	 * @param document
	 */
	public void addDocument(Document document);

	/**
	 * cette methode permet de modifier un Document
	 * 
	 * @param document
	 */
	public void updateDocument(Document document);

	/**
	 * cette methode permet de chercher un Document en utilisant l'Id
	 * 
	 * @param idDocument
	 * @return Document
	 */
	public Document findByIdDocument(int idDocument);

	/**
	 * cette methode permet de supprimer un Document
	 * 
	 * @param Document
	 */
	public void deleteDocument(Document document);
	
	public List<Document> findAll();

}
