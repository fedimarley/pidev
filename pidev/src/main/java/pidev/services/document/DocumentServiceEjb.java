package pidev.services.document;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pidev.domain.Document;

/**
 * Session Bean implementation class DocumentServiceEjb
 * 
 * Cette classe implémente tous les méthodes associées à un Document
 * 
 * @author Fedi
 */
@Stateless
@LocalBean
public class DocumentServiceEjb implements DocumentServiceEjbRemote,
		DocumentServiceEjbLocal {

	@PersistenceContext(unitName = "pidev")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public DocumentServiceEjb() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addDocument(Document document) {
		em.persist(document);
	}

	@Override
	public void updateDocument(Document document) {
		em.merge(document);
	}

	@Override
	public Document findByIdDocument(int idDocument) {
		return em.find(Document.class, idDocument);
	}

	@Override
	public void deleteDocument(Document document) {
		em.remove(document);
	}

	@Override
	public List<Document> findAll() {
		return em.createQuery("select d from Document d").getResultList();
	}

}
