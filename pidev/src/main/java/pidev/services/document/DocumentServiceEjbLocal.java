package pidev.services.document;

import java.util.List;

import javax.ejb.Local;

import pidev.domain.Document;

@Local
public interface DocumentServiceEjbLocal {

	List<Document> findAll();

}
