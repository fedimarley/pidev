package pidev.services.matche;

import javax.ejb.Remote;

import pidev.domain.Matche;

@Remote
public interface MatcheServiceEjbRemote {

	/**
	 * AJOUTER UN MATCH
	 * @param match
	 */
	public void addMatche(Matche match);
	/**
	 * mODIFIER UN MATCH
	 * @param match
	 */
	public void updateMatche(Matche match);
	/**
	 * CHERCHER UN MATCH A PARTIR DE  L IDENTIFIANT
	 * @param Id_Match
	 * @return
	 */
	public Matche findByIdMatch(int Id_Match);
	/**
	 * SUPPRIMER UN MATCH 
	 * @param match
	 */
	public void deleteMatch(Matche match);
}
