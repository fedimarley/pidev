package pidev.services.matche;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pidev.domain.Matche;

/**
 * Session Bean implementation class MatcheServiceEjb
 */
@Stateless
@LocalBean
public class MatcheServiceEjb implements MatcheServiceEjbRemote, MatcheServiceEjbLocal {

	@PersistenceContext(unitName="pidev")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public MatcheServiceEjb() {
        
    }

	@Override
	public void addMatche(Matche match) {
		em.persist(match);
	}

	@Override
	public void updateMatche(Matche match) {
		em.merge(match);
		
	}

	@Override
	public Matche findByIdMatch(int Id_Match) {
		Matche m = em.find(Matche.class, Id_Match);
		return m;
	}

	@Override
	public void deleteMatch(Matche match) {
		em.remove(em.merge(match));
		
	}

}
