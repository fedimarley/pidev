package pidev.services.event;

import java.util.List;

import javax.ejb.Remote;

import pidev.domain.Event;

/**
 * Declaration des methodes associées a un Event
 * 
 * @author Fedi
 *
 */
@Remote
public interface EventServiceEjbRemote {

	/**
	 * cette methode permet d'ajouter un nouveau Event
	 * 
	 * @param event
	 */
	public void addEvent(Event event);

	/**
	 * cette methode permet de modifier un Event
	 * 
	 * @param event
	 */
	public void updateEvent(Event event);

	/**
	 * cette methode permet de chercher un Event en utilisant l'Id
	 * 
	 * @param idEvent
	 * @return Event
	 */
	public Event findByIdEvent(int idEvent);

	/**
	 * cette methode permet de supprimer un Event
	 * 
	 * @param Event
	 */
	public void deleteEvent(Event event);
	
	public List<Event> findAll();

}
