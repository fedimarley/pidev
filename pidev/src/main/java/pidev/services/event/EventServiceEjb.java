package pidev.services.event;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pidev.domain.Event;

/**
 * Session Bean implementation class EventServiceEjb
 * 
 * Cette classe implémente tous les méthodes associées à un Event
 * 
 * @author Fedi
 */
@Stateless
@LocalBean
public class EventServiceEjb implements EventServiceEjbRemote,
		EventServiceEjbLocal {

	@PersistenceContext(unitName = "pidev")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public EventServiceEjb() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addEvent(Event event) {
		em.persist(event);

	}

	@Override
	public void updateEvent(Event event) {
		em.merge(event);
	}

	@Override
	public Event findByIdEvent(int idEvent) {
		return em.find(Event.class, idEvent);
	}

	@Override
	public void deleteEvent(Event event) {
		em.remove(event);
	}

	@Override
	public List<Event> findAll() {
		return em.createQuery("select e from Event e").getResultList();
	}

}
