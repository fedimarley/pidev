package pidev.services.competition;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pidev.domain.Competition;

/**
 * Session Bean implementation class CompetitionServiceEjb
 */
@Stateless
@LocalBean
public class CompetitionServiceEjb implements CompetitionServiceEjbRemote, CompetitionServiceEjbLocal {

	@PersistenceContext(unitName="pidev")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public CompetitionServiceEjb() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addCompetition(Competition competition) {
		em.persist(competition);
		
	}

	@Override
	public void updateCompetition(Competition competition) {
		em.merge(competition);
		
	}

	@Override
	public Competition finByIdCompetition(int Id_Competition) {
		Competition c = em.find(Competition.class, Id_Competition);
		return c;
	}

	@Override
	public void deleteCompetition(Competition competition) {
		em.remove(em.merge(competition));
		
	}

}
