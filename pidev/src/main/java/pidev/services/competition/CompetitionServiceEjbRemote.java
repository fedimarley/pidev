package pidev.services.competition;

import javax.ejb.Remote;

import pidev.domain.Competition;

/**
 * 
 * @author Laabidi
 *
 */
@Remote
public interface CompetitionServiceEjbRemote {
	/**
	 * ajouer une competition
	 * @param competition
	 */
	public void addCompetition(Competition competition);
	/**
	 * modofier une competition
	 * @param competition
	 */
	public void updateCompetition(Competition competition);
	/**
	 * trouver une competition avec l id
	 * @param Id_Competition
	 * @return
	 */
	public Competition finByIdCompetition(int Id_Competition);
	/**
	 * supprimer une copetition
	 * @param competition
	 */
	public void deleteCompetition(Competition competition);

}
