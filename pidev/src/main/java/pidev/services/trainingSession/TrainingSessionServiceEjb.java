package pidev.services.trainingSession;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pidev.domain.Referee;
import pidev.domain.TrainingSession;


/**
 * Session Bean implementation class TrainingSessionServiceEjb
 */
@Stateless
@LocalBean
public class TrainingSessionServiceEjb implements TrainingSessionServiceEjbRemote, TrainingSessionServiceEjbLocal {

    
	@PersistenceContext(unitName = "pidev")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public TrainingSessionServiceEjb() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void addTrainingSession(TrainingSession trainingSession) {
			em.persist(trainingSession);		
	}

	@Override
	public void updateTrainingSession(TrainingSession trainingSession) {
		em.merge(trainingSession);
		
	}

	@Override
	public TrainingSession findTrainingSessionById(int id) {
	TrainingSession r=em.find(TrainingSession.class,id);
	return r;
	}

	@Override
	public void deleteTrainingSession(TrainingSession trainingSession) {
		em.remove(em.merge(trainingSession));		
	}

	@Override
	public List<TrainingSession> findAll() {
		
		return em.createQuery("select r from TrainingSession r").getResultList();
		
	}


}
