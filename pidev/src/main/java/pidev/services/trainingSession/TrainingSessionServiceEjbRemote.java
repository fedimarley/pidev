package pidev.services.trainingSession;

import java.util.List;

import javax.ejb.Remote;

import pidev.domain.Referee;
import pidev.domain.TrainingSession;

@Remote
public interface TrainingSessionServiceEjbRemote {
	/**
	 * this methode adds a Training Session
	 * @param TrainingSession
	 */
	public void addTrainingSession(TrainingSession TrainingSession);
	/**
	 * this methode edit a Training Session
	 * @param TrainingSession
	 */
	public void updateTrainingSession(TrainingSession TrainingSession);
	/**
	 * this methode find by id a Training Session
	 * @param id
	 * @return
	 */
	public TrainingSession findTrainingSessionById(int id);
	/**
	 * this methode remove a Training Session
	 * @param personne
	 */
	public void deleteTrainingSession(TrainingSession TrainingSession);
	public List<TrainingSession> findAll();
}
