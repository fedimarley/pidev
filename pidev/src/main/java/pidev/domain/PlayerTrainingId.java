package pidev.domain;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class PlayerTrainingId implements Serializable {
	/**
	 * cette classe constitue la clé primère composé de la classe associative entre Player et Training
	 * @author Fedi
	 */
	private static final long serialVersionUID = 1L;
	private int Id_Player;
	private int Id_Training;

	public int getId_Player() {
		return Id_Player;
	}

	public void setId_Player(int id_Player) {
		Id_Player = id_Player;
	}

	public int getId_Training() {
		return Id_Training;
	}

	public void setId_Training(int id_Training) {
		Id_Training = id_Training;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id_Player;
		result = prime * result + Id_Training;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayerTrainingId other = (PlayerTrainingId) obj;
		if (Id_Player != other.Id_Player)
			return false;
		if (Id_Training != other.Id_Training)
			return false;
		return true;
	}

}
