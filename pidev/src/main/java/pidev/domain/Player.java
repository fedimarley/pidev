package pidev.domain;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Player
 * 
 * @author Fedi
 */
@Entity
@Table(name = "t_Player")
public class Player implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int Id_Player;
	private String name;
	private String firstName;
	private int age;
	private String grade;
	private String career;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name = "association_fk")
	private Association association;
	@OneToMany(mappedBy = "player", cascade = CascadeType.ALL)
	private List<PlayerTraining> trainingList = new ArrayList<PlayerTraining>();
	@OneToMany(mappedBy = "player", cascade = CascadeType.ALL)
	private List<PlayerMatch> listMatches = new ArrayList<PlayerMatch>();
	@OneToMany(mappedBy = "player", cascade = CascadeType.ALL)
	private List<PlayerTest> listPlayers = new ArrayList<PlayerTest>();

	public Player() {
		super();
	}

	public int getId_Player() {
		return this.Id_Player;
	}

	public void setId_Player(int Id_Player) {
		this.Id_Player = Id_Player;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGrade() {
		return this.grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getCareer() {
		return this.career;
	}

	public void setCareer(String career) {
		this.career = career;
	}

	public Association getAssociation() {
		return association;
	}

	public void setAssociation(Association association) {
		this.association = association;
	}

	public List<PlayerTraining> getTrainingList() {
		return trainingList;
	}

	public void setTrainingList(List<PlayerTraining> trainingList) {
		this.trainingList = trainingList;
	}

	public List<PlayerMatch> getListMatches() {
		return listMatches;
	}

	public void setListMatches(List<PlayerMatch> listMatches) {
		this.listMatches = listMatches;
	}

}
