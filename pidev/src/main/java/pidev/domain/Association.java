package pidev.domain;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Association
 * @author Fedi
 */
@Entity
@Table(name = "t_Association")
public class Association implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int Id_Association;
	private String name;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy = "association", cascade = CascadeType.ALL)
	private List<Player> players = new ArrayList<Player>();

	public Association() {
		super();
	}

	public int getId_Association() {
		return this.Id_Association;
	}

	public void setId_Association(int Id_Association) {
		this.Id_Association = Id_Association;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

}
