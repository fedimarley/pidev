package pidev.domain;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Document
 * @author Fedi
 */
@Entity
@Table(name = "t_Document")
public class Document implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int Id_Document;
	private String information;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name = "admin_fk")
	private Administrator administrator;

	public Document() {
		super();
	}

	public int getId_Document() {
		return this.Id_Document;
	}

	public void setId_Document(int Id_Document) {
		this.Id_Document = Id_Document;
	}

	public String getInformation() {
		return this.information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public Administrator getAdministrator() {
		return administrator;
	}

	public void setAdministrator(Administrator administrator) {
		this.administrator = administrator;
	}

}
