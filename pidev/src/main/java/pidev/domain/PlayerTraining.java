package pidev.domain;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: PlayerTraining
 * @author Fedi
 */
@Entity
@Table(name="t_PlayerTraining")
public class PlayerTraining implements Serializable {
	@EmbeddedId
	private PlayerTrainingId playerTrainingId;
	private String result;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name = "Id_Player", insertable = false, updatable = false)
	private Player player;
	@ManyToOne
	@JoinColumn(name = "Id_Training", insertable = false, updatable = false)
	private Training training;

	public PlayerTraining() {
		super();
	}

	public String getResult() {
		return this.result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public PlayerTrainingId getPlayerTrainingId() {
		return playerTrainingId;
	}

	public void setPlayerTrainingId(PlayerTrainingId playerTrainingId) {
		this.playerTrainingId = playerTrainingId;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Training getTraining() {
		return training;
	}

	public void setTraining(Training training) {
		this.training = training;
	}

}
