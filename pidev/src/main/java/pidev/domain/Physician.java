package pidev.domain;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Physician
 * 
 * @author Fedi
 */
@Entity
@Table(name="t_physician")
public class Physician implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Id_Physician;
	private String name;
	private String firstName;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy = "physician", cascade = CascadeType.ALL)
	private List<PlayerTest> listPhysician = new ArrayList<PlayerTest>();
	@ManyToOne
	@JoinColumn(name = "rsponsible_fk")
	private AntiDopingResponsible antiDopingResponsible;

	public Physician() {
		super();
	}

	public int getId_Physician() {
		return this.Id_Physician;
	}

	public void setId_Physician(int Id_Physician) {
		this.Id_Physician = Id_Physician;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

}
