package pidev.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Matche
 * 
 * @author Fedi
 */
@Entity
@Table(name = "t_Matche")
public class Matche implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int Id_Match;
	private String date;
	private String time;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name = "stadium_fk")
	private Stadium stadium;
	@ManyToOne
	@JoinColumn(name = "competition_fk")
	private Competition competition;
	@ManyToOne
	@JoinColumn(name = "referee_fk")
	private Referee referee;
	@OneToMany(mappedBy = "matche", cascade = CascadeType.ALL)
	private List<PlayerMatch> listPlayers = new ArrayList<PlayerMatch>();

	public Matche() {
		super();
	}

	public int getId_Match() {
		return this.Id_Match;
	}

	public void setId_Match(int Id_Match) {
		this.Id_Match = Id_Match;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return this.time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Stadium getStadium() {
		return stadium;
	}

	public void setStadium(Stadium stadium) {
		this.stadium = stadium;
	}

	public List<PlayerMatch> getListPlayers() {
		return listPlayers;
	}

	public void setListPlayers(List<PlayerMatch> listPlayers) {
		this.listPlayers = listPlayers;
	}

	public Competition getCompetition() {
		return competition;
	}

	public void setCompetition(Competition competition) {
		this.competition = competition;
	}

	public Referee getReferee() {
		return referee;
	}

	public void setReferee(Referee referee) {
		this.referee = referee;
	}

}
