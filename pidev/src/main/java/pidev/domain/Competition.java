package pidev.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Competition
 * @author Fedi
 */
@Entity
@Table(name = "t_Competition")
public class Competition implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int Id_Competition;
	private String startDate;
	private String endDate;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy = "competition", cascade = CascadeType.ALL)
	private List<Matche> matches = new ArrayList<Matche>();
	@ManyToOne
	@JoinColumn(name = "admin_fk")
	private Administrator administrator;

	public Competition() {
		super();
	}

	public int getId_Competition() {
		return this.Id_Competition;
	}

	public void setId_Competition(int Id_Competition) {
		this.Id_Competition = Id_Competition;
	}

	public String getStartDate() {
		return this.startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return this.endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public List<Matche> getMatches() {
		return matches;
	}

	public void setMatches(List<Matche> matches) {
		this.matches = matches;
	}

	public Administrator getAdministrator() {
		return administrator;
	}

	public void setAdministrator(Administrator administrator) {
		this.administrator = administrator;
	}

}
