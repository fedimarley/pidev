package pidev.domain;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class PlayerTestId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int Id_Player;
	private int Id_Physician;

	public PlayerTestId() {
		super();
	}

	public int getId_Player() {
		return Id_Player;
	}

	public void setId_Player(int id_Player) {
		Id_Player = id_Player;
	}

	public int getId_Physician() {
		return Id_Physician;
	}

	public void setId_Physician(int id_Physician) {
		Id_Physician = id_Physician;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id_Physician;
		result = prime * result + Id_Player;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayerTestId other = (PlayerTestId) obj;
		if (Id_Physician != other.Id_Physician)
			return false;
		if (Id_Player != other.Id_Player)
			return false;
		return true;
	}

}
