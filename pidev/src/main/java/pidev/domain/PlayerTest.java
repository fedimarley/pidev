package pidev.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: PlayerTest
 *
 * @author Fedi
 */
@Entity
@Table(name = "t_playerTest")
public class PlayerTest implements Serializable {
	@EmbeddedId
	private PlayerTestId playerTestId;
	private String date;
	private String time;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name = "Id_Physician", insertable = false, updatable = false)
	private Physician physician;
	@ManyToOne
	@JoinColumn(name = "Id_Player", insertable = false, updatable = false)
	private Player player;

	public PlayerTest() {
		super();
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return this.time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}
