package pidev.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Training
 * 
 * @author Fedi
 */
@Entity
@Table(name = "t_Training")
public class Training implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int Id_Training;
	private String date;
	private String Time;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name = "admin_fk")
	private Administrator administrator;
	@OneToMany(mappedBy = "training", cascade = CascadeType.ALL)
	private List<PlayerTraining> trainingList = new ArrayList<PlayerTraining>();

	public Training() {
		super();
	}

	public int getId_Training() {
		return this.Id_Training;
	}

	public void setId_Training(int Id_Training) {
		this.Id_Training = Id_Training;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return Time;
	}

	public void setTime(String time) {
		Time = time;
	}

	public Administrator getAdministrator() {
		return administrator;
	}

	public void setAdministrator(Administrator administrator) {
		this.administrator = administrator;
	}

	public List<PlayerTraining> getTrainingList() {
		return trainingList;
	}

	public void setTrainingList(List<PlayerTraining> trainingList) {
		this.trainingList = trainingList;
	}

}
