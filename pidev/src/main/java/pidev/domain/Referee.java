package pidev.domain;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Referee
 * @author Fedi
 */
@Entity
@Table(name="t_Referee")
public class Referee implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Id_Referee;
	private String name;
	private String firstName;
	private String grade;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy = "referee", cascade = CascadeType.ALL)
	private List<Matche> matches = new ArrayList<Matche>();
	@OneToMany(mappedBy = "referee", cascade = CascadeType.ALL)
	private List<RefereeTrainingSession> listTraining = new ArrayList<RefereeTrainingSession>();

	public Referee() {
		super();
	}

	public int getId_Referee() {
		return this.Id_Referee;
	}

	public void setId_Referee(int Id_Referee) {
		this.Id_Referee = Id_Referee;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGrade() {
		return this.grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public List<Matche> getMatches() {
		return matches;
	}

	public void setMatches(List<Matche> matches) {
		this.matches = matches;
	}

	public List<RefereeTrainingSession> getListTraining() {
		return listTraining;
	}

	public void setListTraining(List<RefereeTrainingSession> listTraining) {
		this.listTraining = listTraining;
	}

}
