package pidev.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: TrainingSession
 * @author Fedi
 */
@Entity
@Table(name="t_TriningSession")
public class TrainingSession implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Id_Session;
	private String date;
	private String time;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy = "trainingSession", cascade = CascadeType.ALL)
	private List<RefereeTrainingSession> listReferee = new ArrayList<RefereeTrainingSession>();
	@ManyToOne
	@JoinColumn(name = "admin_fk")
	private Administrator administrator;

	public TrainingSession() {
		super();
	}

	public int getId_Session() {
		return this.Id_Session;
	}

	public void setId_Session(int Id_Session) {
		this.Id_Session = Id_Session;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return this.time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public List<RefereeTrainingSession> getListReferee() {
		return listReferee;
	}

	public void setListReferee(List<RefereeTrainingSession> listReferee) {
		this.listReferee = listReferee;
	}

	public Administrator getAdministrator() {
		return administrator;
	}

	public void setAdministrator(Administrator administrator) {
		this.administrator = administrator;
	}

}
