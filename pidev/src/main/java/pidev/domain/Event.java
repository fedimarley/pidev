package pidev.domain;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Event
 * @author Fedi
 */
@Entity
@Table(name = "t_Event")
public class Event implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int Id_Event;
	private String type;
	private String information;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name = "admin_fk")
	private Administrator administrator;

	public Event() {
		super();
	}

	public int getId_Event() {
		return this.Id_Event;
	}

	public void setId_Event(int Id_Event) {
		this.Id_Event = Id_Event;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getInformation() {
		return this.information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public Administrator getAdministrator() {
		return administrator;
	}

	public void setAdministrator(Administrator administrator) {
		this.administrator = administrator;
	}

}
