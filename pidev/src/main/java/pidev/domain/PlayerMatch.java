package pidev.domain;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: PlayerMatch
 * 
 * @author Fedi
 */
@Entity
@Table(name = "t_PlayerMatch")
public class PlayerMatch implements Serializable {
	@EmbeddedId
	private PlayerMatchId playerMatchId;
	private String resultMatch;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name = "Id_Player", insertable = false, updatable = false)
	private Player player;
	@ManyToOne
	@JoinColumn(name = "Id_Match", insertable = false, updatable = false)
	private Matche matche;

	public PlayerMatch() {
		super();
	}

	public String getResultMatch() {
		return this.resultMatch;
	}

	public void setResultMatch(String resultMatch) {
		this.resultMatch = resultMatch;
	}

}
