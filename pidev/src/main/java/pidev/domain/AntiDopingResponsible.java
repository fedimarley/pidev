package pidev.domain;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: AntiDopingResponsible
 *
 * @author Fedi
 */
@Entity
@Table(name = "t_antiDopingResponsible")
public class AntiDopingResponsible implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Id_Responsible;
	private String name;
	private String firstName;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy = "antiDopingResponsible", cascade = CascadeType.ALL)
	private List<Physician> physicians = new ArrayList<Physician>();

	public AntiDopingResponsible() {
		super();
	}

	public int getId_Responsible() {
		return this.Id_Responsible;
	}

	public void setId_Responsible(int Id_Responsible) {
		this.Id_Responsible = Id_Responsible;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

}
