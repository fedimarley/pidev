package pidev.domain;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Stadium
 * @author Fedi
 */
@Entity
@Table(name = "t_Stadium")
public class Stadium implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int Id_Staduim;
	private String name;
	private String region;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy = "stadium", cascade = CascadeType.ALL)
	private List<Matche> matches = new ArrayList<Matche>();

	public Stadium() {
		super();
	}

	public int getId_Staduim() {
		return this.Id_Staduim;
	}

	public void setId_Staduim(int Id_Staduim) {
		this.Id_Staduim = Id_Staduim;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public List<Matche> getMatches() {
		return matches;
	}

	public void setMatches(List<Matche> matches) {
		this.matches = matches;
	}

}
