package pidev.domain;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: RefereeTrainingSession
 * 
 * @author Fedi
 */
@Entity
@Table(name = "t_RefereeTrainingSession")
public class RefereeTrainingSession implements Serializable {
	@EmbeddedId
	private RefereeTrainingSessionId refereeTrainingSessionId;
	private String resultSession;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name = "Id_Referee", insertable = false, updatable = false)
	private Referee referee;
	@ManyToOne
	@JoinColumn(name = "Id_Session", insertable = false, updatable = false)
	private TrainingSession trainingSession;

	public RefereeTrainingSession() {
		super();
	}

	public String getResultSession() {
		return this.resultSession;
	}

	public void setResultSession(String resultSession) {
		this.resultSession = resultSession;
	}

	public RefereeTrainingSessionId getRefereeTrainingSessionId() {
		return refereeTrainingSessionId;
	}

	public void setRefereeTrainingSessionId(
			RefereeTrainingSessionId refereeTrainingSessionId) {
		this.refereeTrainingSessionId = refereeTrainingSessionId;
	}

	public Referee getReferee() {
		return referee;
	}

	public void setReferee(Referee referee) {
		this.referee = referee;
	}

	public TrainingSession getTrainingSession() {
		return trainingSession;
	}

	public void setTrainingSession(TrainingSession trainingSession) {
		this.trainingSession = trainingSession;
	}

}
