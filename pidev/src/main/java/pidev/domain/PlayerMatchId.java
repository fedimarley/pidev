package pidev.domain;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class PlayerMatchId implements Serializable {
	/**
	 * cette classe constitue la clé primère composé de la classe associative entre Player et Matche
	 * @author Fedi
	 */
	private static final long serialVersionUID = 1L;
	private int Id_Player;
	private int Id_Match;

	public PlayerMatchId() {
		super();
	}

	public int getId_Player() {
		return Id_Player;
	}

	public void setId_Player(int id_Player) {
		Id_Player = id_Player;
	}

	public int getId_Match() {
		return Id_Match;
	}

	public void setId_Match(int id_Match) {
		Id_Match = id_Match;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id_Match;
		result = prime * result + Id_Player;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayerMatchId other = (PlayerMatchId) obj;
		if (Id_Match != other.Id_Match)
			return false;
		if (Id_Player != other.Id_Player)
			return false;
		return true;
	}

}
