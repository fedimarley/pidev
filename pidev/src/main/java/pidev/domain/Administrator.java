package pidev.domain;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Administrator
 * @author Fedi
 */
@Entity
@Table(name = "t_Administrator")
public class Administrator implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int Id_Admin;
	private String name;
	private String firstName;
	private String login;
	private String password;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy = "administrator", cascade = CascadeType.ALL)
	private List<Document> documents = new ArrayList<Document>();
	@OneToMany(mappedBy = "administrator", cascade = CascadeType.ALL)
	private List<Event> events = new ArrayList<Event>();
	@OneToMany(mappedBy = "administrator", cascade = CascadeType.ALL)
	private List<Training> trainings = new ArrayList<Training>();
	@OneToMany(mappedBy = "administrator", cascade = CascadeType.ALL)
	private List<Competition> competitions = new ArrayList<Competition>();
	@OneToMany(mappedBy = "administrator", cascade = CascadeType.ALL)
	private List<TrainingSession> trainingSessions = new ArrayList<TrainingSession>();

	public Administrator() {
		super();
	}

	public int getId_Admin() {
		return this.Id_Admin;
	}

	public void setId_Admin(int Id_Admin) {
		this.Id_Admin = Id_Admin;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public List<Training> getTrainings() {
		return trainings;
	}

	public void setTrainings(List<Training> trainings) {
		this.trainings = trainings;
	}

	public List<Competition> getCompetitions() {
		return competitions;
	}

	public void setCompetitions(List<Competition> competitions) {
		this.competitions = competitions;
	}

	public List<TrainingSession> getTrainingSessions() {
		return trainingSessions;
	}

	public void setTrainingSessions(List<TrainingSession> trainingSessions) {
		this.trainingSessions = trainingSessions;
	}

}
