package pidev.domain;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class RefereeTrainingSessionId implements Serializable {
	/**
	 * cette classe constitue la clé primère composé de la classe associative entre Referee et TrainingSession
	 * @author Fedi
	 */
	private static final long serialVersionUID = 1L;
	private int Id_Referee;
	private int Id_Session;

	public int getId_Referee() {
		return Id_Referee;
	}

	public void setId_Referee(int id_Referee) {
		Id_Referee = id_Referee;
	}

	public int getId_Session() {
		return Id_Session;
	}

	public void setId_Session(int id_Session) {
		Id_Session = id_Session;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id_Referee;
		result = prime * result + Id_Session;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RefereeTrainingSessionId other = (RefereeTrainingSessionId) obj;
		if (Id_Referee != other.Id_Referee)
			return false;
		if (Id_Session != other.Id_Session)
			return false;
		return true;
	}

}
